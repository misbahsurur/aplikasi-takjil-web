<?php 
	include("koneksi.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Web Page Aplikasi x0b</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="index.php">Aplikasi X0b Web</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Menu
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                <a class="dropdown-item" type="button" href="takjil.php">Tabel Takjil</a>
            </div>
        </div>
    </nav>
    <div class="container">
        <header>
            <h1><center>Daftar takjil<center></h1>
        </header>
        <table class="table table-bordered table-hover">
		    <thead>
			    <tr>
				    <th>No</th>
				    <th>Nama</th>
				    <th>Harga</th>
                    <th>Foto</th>
                    <th>Action</th>
			    </tr>
		    </thead>
            <tbody>
			    <?php 
				    $ambil = $db->query("SELECT * FROM menu ORDER BY id");
                   // $query = mysqli_query($db,$sql);
                    $no=1;
                    while ($menu = $ambil->fetch_assoc()) {
                        ?>
                        <tr>
                        <td><?php echo $menu['id'] ?></td>
                        <td><?php echo $menu['nama'] ?></td>
                        <td><?php echo $menu['harga'] ?></td>
                        <td><img src='../images/<?php echo $menu['images'] ?>' width='50' height='50' /></td>
                        <td>
						    <div class='btn-group'>
                                <a href="hapus_menu.php?id=<?php echo $menu['id']?>" title="Hapus Data Ini" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA PENTING INI ... ?')">
                                    <button class="btn btn-xs btn-danger tipsy-kiri-atas"><i class="icon-remove icon-white"></i> Hapus</button>
                                </a> 
						    </div>
						</td>
                        </tr>
                        <?php
                    }
                    ?>
            </tbody>
        </table>
    </div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>