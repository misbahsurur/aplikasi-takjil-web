/*
 Navicat Premium Data Transfer

 Source Server         : okymarsel
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : takjil

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 14/05/2020 15:08:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `images` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Es Poci', 3000, 'poci.jpg');
INSERT INTO `menu` VALUES ('2', 'Es Buah', 5000, 'esbuah.jpg');
INSERT INTO `menu` VALUES ('3', 'Es Kelapa', 3500, 'kelapa.jpg');
INSERT INTO `menu` VALUES ('4', 'Sosis Bakar', 2500, 'sosis.jpg');
INSERT INTO `menu` VALUES ('5', 'Aneka Gorengan', 500, 'gorengan.jpg');
INSERT INTO `menu` VALUES ('6', 'Aneka Kurma', 20000, 'kurma.jpg');

SET FOREIGN_KEY_CHECKS = 1;
